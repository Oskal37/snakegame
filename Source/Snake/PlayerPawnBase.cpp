// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeActor.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();

}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::PlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::PlayerHorizontalInput);

}

void APlayerPawnBase::CreateSnakeActor()
{
	Snake = GetWorld()->SpawnActor<ASnakeActor>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::PlayerVerticalInput(float value)
{
	if (IsValid(Snake))
	{
		if (value > 0 && Snake->LastMoveDirection != EDirectionMove::DOWN)
		{
			Snake->LastMoveDirection = EDirectionMove::UP;
		}
		else
			if (value < 0 && Snake->LastMoveDirection != EDirectionMove::UP)
			{
				Snake->LastMoveDirection = EDirectionMove::DOWN;
			}
	}
}

void APlayerPawnBase::PlayerHorizontalInput(float value)
{
	
	if (IsValid(Snake))
	{		
		if (value > 0 && Snake->LastMoveDirection != EDirectionMove::RIGHT)
		{
			Snake->LastMoveDirection = EDirectionMove::LEFT;
		}
		else
			if (value < 0 && Snake->LastMoveDirection != EDirectionMove::LEFT)
			{
				
				Snake->LastMoveDirection = EDirectionMove::RIGHT;
			}
	}
}





