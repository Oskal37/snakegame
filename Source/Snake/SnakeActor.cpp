// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeActor.h"
#include "SnakeElement.h"

// Sets default values
ASnakeActor::ASnakeActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	SpeedMove = 0.5f;

}

// Called when the game starts or when spawned
void ASnakeActor::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(6);
}

// Called every frame
void ASnakeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(SpeedMove);
	Move();
}

void ASnakeActor::AddSnakeElement(int ElementNum)
{
	for (int i = 0; i < ElementNum; i++) 
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(GetActorLocation() - NewLocation);
		ASnakeElement* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElement>(SnakeElementClass, NewTransform);
		int32 IndexElement = SnakeElements.Add(NewSnakeElem);
		if (IndexElement == 0) 
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeActor::Move()
{
	FVector MovementVector(ForceInitToZero);	

	switch (LastMoveDirection)
	{
	case EDirectionMove::UP:
		MovementVector.X += ElementSize;
		break;
	case EDirectionMove::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EDirectionMove::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	case EDirectionMove::RIGHT:
		MovementVector.Y += ElementSize;
		break;
	}

	for (int i = SnakeElements.Num() - 1; i > 0; i--) 
	{
		FVector PrevLocation = SnakeElements[i - 1]->GetActorLocation();
		SnakeElements[i]->SetActorLocation(PrevLocation);
	}
	
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
}

