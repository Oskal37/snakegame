// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeActor.generated.h"

class ASnakeElement;

UENUM()
enum class EDirectionMove 
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKE_API ASnakeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeActor();

	UPROPERTY(EditDefaultsOnly);
	TSubclassOf<ASnakeElement> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly);
	float ElementSize;

	UPROPERTY();
	TArray<ASnakeElement*> SnakeElements;
	
	UPROPERTY(EditDefaultsOnly);
	float SpeedMove;

	UPROPERTY();
    EDirectionMove LastMoveDirection;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementNum = 1);

	void Move();
};
